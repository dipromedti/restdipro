class DipromedController < ApplicationController

  def monedas
    render :json => Monedas.all
  end

  def suc
		render :json => Tabsu.all
 	end

 	def maestro
		render :json => Maeedo.all
 	end

 	def menu
 		consulta = "select * from tabpa"
 		render :json => ActiveRecord::Base.connection.exec_query(consulta)
 	end

  def paises
    cursor = ActiveRecord::Base.connection.raw_connection.parse("BEGIN BARLOVENTO.COMBO_PAISES(:datos);END;")
    cursor.bind_param(':datos', nil, OCI8::Cursor )
    cursor.exec()
    results_cursor = cursor[':datos']
    cursor.close
    data=Array.new
    results_cursor.fetch_hash do |row|
      data.push(row)
      end
    render :json => data
    results_cursor.close
  end

  def login
    cursor = ActiveRecord::Base.connection.raw_connection.parse("BEGIN INTRAWEB.CONSULTA_USR('"+params[:PRUT]+"','"+params[:PDIGITO]+"','"+params[:PCLAVE]+"',:datos);END;")
    cursor.bind_param(':datos', nil, OCI8::Cursor )
    cursor.exec()
    results_cursor = cursor[':datos']
    cursor.close
    data=Array.new
    results_cursor.fetch_hash do |row|
      data.push(row)
    end
    render :json => data
    results_cursor.close
  end

  def logeando
    @existe= Tabfu.where(RTFU: params[:rtfu],DIGITO:params[:digito],PWFU:params[:pwfu],ACTIVO:'S').exists?
    render :json => @existe
  end

  def vasa
    cursor = ActiveRecord::Base.connection.raw_connection.parse("BEGIN PRODUCCION.PACK_REPORTES.VTASAGNOSEMANAANTERIOR('"+params[:fech]+"',:datos);END;")
    cursor.bind_param(':datos', nil, OCI8::Cursor )
    cursor.exec()
    results_cursor = cursor[':datos']
    cursor.close
    data=Array.new
    results_cursor.fetch_hash do |row|
      data.push(row)
    end
    render :json => data
    results_cursor.close
  end

  def vtaant
    cursor = ActiveRecord::Base.connection.raw_connection.parse("BEGIN PRODUCCION.PACK_REPORTES.VTASAGNOANTERIOR('"+params[:fech]+"',:datos);END;")
    cursor.bind_param(':datos', nil, OCI8::Cursor )
    cursor.exec()
    results_cursor = cursor[':datos']
    cursor.close
    data=Array.new
    results_cursor.fetch_hash do |row|
      data.push(row)
    end
    render :json => data
    results_cursor.close
  end

  def vtafamilia
    cursor = ActiveRecord::Base.connection.raw_connection.parse("BEGIN PRODUCCION.PACK_REPORTES.VTASFAMILIA('"+params[:fech]+"',:datos);END;")
    cursor.bind_param(':datos', nil, OCI8::Cursor )
    cursor.exec()
    results_cursor = cursor[':datos']
    cursor.close
    data=Array.new
    results_cursor.fetch_hash do |row|
      data.push(row)
    end
    render :json => data
    results_cursor.close
  end
end

