Rails.application.routes.draw do
  get 'dipromed/monedas'
  put 'dipromed/monedas'
  get 'dipromed/suc'
  get 'dipromed/maestro'
  get 'dipromed/paises'
  get 'dipromed/menu'
  get 'dipromed/login'
  put 'dipromed/login'
  get 'dipromed/logeando'
  put 'dipromed/logeando'
  get 'dipromed/vasa'
  put 'dipromed/vasa'
  get 'dipromed/vtaant'
  put 'dipromed/vtaant'
  get 'dipromed/vtafamilia'
  put 'dipromed/vtafamilia'
  
end